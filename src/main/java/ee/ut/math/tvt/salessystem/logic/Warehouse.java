package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import java.util.List;

public class Warehouse {
    List<StockItem> allItems;

    private final SalesSystemDAO dao;

    public Warehouse(List<StockItem> allItems, SalesSystemDAO dao) {
        this.allItems = allItems;
        this.dao = dao;
    }

    public void addItem() {
    }

    public boolean changeStock(StockItem item, int quantity) {
        if (quantity > 0) {
            return true;
        }
        return false;
    }

    List<SoldItem> getAll() {
        return null;
    }

    public boolean enough(StockItem item, int quantity) {
        return false;
    }

    public boolean exists(long id) {
        return dao.findStockItem(id) != null;
    }

    public int quantity(long id) {
        StockItem stockItem;
        stockItem = dao.findStockItem(id);
        return stockItem.getQuantity();
    }

    public void addToWarehouse(StockItem item){
        dao.saveStockItem(item);
    }

    public void addToWarehouse(Long id, String name, String description, double price, int i) {
        StockItem item = new StockItem(id, name, description, price, i);
        dao.saveStockItem(item);
    }
}
