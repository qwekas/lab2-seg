package ee.ut.math.tvt.salessystem.dataobjects;

/**
 * Created by John on 6.11.2017.
 */
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="Sale")
public class Sale {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany(cascade = CascadeType.ALL)
    private List<SoldItem> solditem;
    @Column(name = "date")
    private Date date;
    @Column(name = "time")
    private Time time;

    public Sale() {
    }

    public Sale(List<SoldItem> solditem, Date date) {
        this.solditem = solditem;
        this.date = date;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public List<SoldItem> getSolditem() {
        return solditem;
    }
    public void setSolditem(List<SoldItem> solditem) {
        this.solditem = solditem;
    }
    public Date getDate() {return date;}
    public void setDate(Date date) {this.date = date;}

}