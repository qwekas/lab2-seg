package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Sale;

import java.time.LocalDate;
import java.util.List;

public class History {

    private final SalesSystemDAO dao;

    public History(SalesSystemDAO dao) {
        this.dao = dao;
    }

    private void addItem(Sale sale){
        dao.saveSale(sale);
    }

    public Sale get(LocalDate date) {return null;}

    public List<Sale> getBetween(LocalDate date1, LocalDate date2){
        return null;
    }

    public List<Sale> getLastTen(){
        return null;
    }

    //public List<Sale> getAll(){
        //return dao.getAllSales();
    //}
    //Only if need todz get all sales history
}
