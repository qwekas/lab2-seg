package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.Sale;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class HibernateSalesSystemDAO implements SalesSystemDAO {

    private final EntityManagerFactory emf;
    private final EntityManager em;

    public HibernateSalesSystemDAO() {
        // if you get ConnectException/JDBCConnectionException then you
        // probably forgot to start the database before starting the application
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();
    }

    public void close() {
        em.close();
        emf.close();
    }

    @Override
    public List<StockItem> findAllStockItems() {
        return em.createQuery("From StockItem", StockItem.class).getResultList();
    }

    @Override
    public StockItem findStockItem(long id) {
        StockItem item  = null;
        try {
            item = em.createQuery("From StockItem where Id=" + id, StockItem.class).getSingleResult();
        }catch (Exception e) {
            throw new RuntimeException(e);
        }finally {
            return item;
        }
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        beginTransaction();
        em.merge(stockItem);
        if(stockItem.getQuantity() == 0)
            em.remove(stockItem);
        commitTransaction();
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        em.merge(item);
    }

    @Override
    public void beginTransaction() {
        em.getTransaction().begin();
    }

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }

    public void saveSale(Sale sale){
        beginTransaction();
        em.persist(sale);
        commitTransaction();
    }

    public List<Sale> getAllSales(){
        return em.createQuery("FROM Sale",Sale.class).getResultList();
    }
}