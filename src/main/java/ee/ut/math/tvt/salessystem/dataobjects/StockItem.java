package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Stock item.
 */
@Entity
@Table(name = "stock_item")

public class StockItem implements Serializable {
    @Id
    @Column(name="id")
    private Long id;
    @Column(name = "name")
    private String name;
	@Column(name = "price")
    private double price;
    @Column(name = "description")
	private String description;
    @Column(name = "quantity")
	private int quantity;

    public StockItem() {
    }

    public StockItem(Long id, String name, String desc, double price, int quantity) {
        this.id = id;
        this.name = name;
        this.description = desc;
        this.price = price;
        this.quantity = quantity;
    }

    @OneToOne
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    @OneToOne
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @OneToOne
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    @OneToOne
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @OneToOne
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return String.format("StockItem{id=%d, name='%s'}", id, name);
    }

}
