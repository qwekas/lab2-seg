package ee.ut.math.tvt.salessystem;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CartTest {

    private InMemorySalesSystemDAO dao;

    ShoppingCart sc;
    StockItem st;
    SoldItem soldZero;
    SoldItem soldNeg;
    SoldItem soldPos;

    @Before
    public void setUp() {
        dao = new InMemorySalesSystemDAO();
        sc = new ShoppingCart(dao);
        st = new StockItem(new Long(1), "Juust", "", 5, 1);
        soldZero = new SoldItem(st, 0);
        soldNeg = new SoldItem(st, -1);
        soldPos = new SoldItem(st, 1);
    }


    @Test
    public void testAddingNewItem() {
        assertFalse(sc.contains(soldPos));
        sc.addItem(soldPos);
        assertTrue(sc.contains(soldPos));
    }

    @Test(expected = Exception.class)
    public void testAddingItemWithNegativeQuantity() {
        sc.addItem(soldNeg);
        assertFalse(sc.contains(soldNeg));
    }

    @Test(expected = Exception.class)
    public void testAddingItemWithZeroQuantity() {
        sc.addItem(soldZero);
        assertFalse(sc.contains(soldZero));
    }

    @Test
    public void testAddingItemWithQuantityTooLarge() {
    }

    @Test
    public void testAddingItemWithQuantitySumTooLarge() {
    }

    @Test
    public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity() {

    }

    @Test
    public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction() {
        sc.submitCurrentPurchase();
        assertFalse(dao.transactionStarted);
        assertTrue(dao.transactionCommited);
    }

    @Test
    public void testSubmittingCurrentOrderCreatesHistoryItem() {

    }

    @Test
    public void testSubmittingCurrentOrderSavesCorrectTime() {

    }

    @Test
    public void testCancellingOrder() {
        sc.cancelCurrentPurchase();
        assertEquals(null, sc.getAll());
    }

    @Test
    public void testCancellingOrderQuanititesUnchanged() {
    }
}
