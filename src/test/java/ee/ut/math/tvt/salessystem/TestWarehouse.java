package ee.ut.math.tvt.salessystem;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestWarehouse {

    private InMemorySalesSystemDAO dao;

    Warehouse wh;
    StockItem st1;
    StockItem st2;
    StockItem st3;
    SoldItem soldItem;
    ShoppingCart sc;

    @Before
    public void setUp() {
        dao = new InMemorySalesSystemDAO();
        wh= new Warehouse(new ArrayList<StockItem>(),dao);
        st1 = new StockItem(new Long(1), "kartul", "", 5, 1);
        st2 = new StockItem(new Long(2), "õun", "", 5, 5);
        st3 = new StockItem(new Long(2), "Lays", "", 5, -1);
        soldItem = new SoldItem(st1, 1);
        sc = new ShoppingCart(dao);
    }
    @Test
    public void testAddingItemBeginsAndCommitsTransaction () {
        wh.addToWarehouse(st1);
        assertTrue(dao.transactionStarted);
        assertTrue(dao.transactionCommited);
    }
    @Test
    public void testAddingNewItem() {
        assertFalse(wh.changeStock(st1, 0));
        wh.addToWarehouse(st1);
        assertTrue(wh.changeStock(st1, 1));
    }

    @Test
    public void testAddingExistingItem () {
        wh.addToWarehouse(st1);
        if (wh.quantity(st1.getQuantity()) >0) {
            wh.addToWarehouse(st1.getId(), st1.getName(), st1.getDescription(),st1.getPrice(), st1.getQuantity()+1);
        }
        else { wh.addToWarehouse(st1);}
        assertEquals(2, wh.quantity(st1.getQuantity()));
    }
    @Test
    public void testAddingItemWithNegativeQuantity () {
        wh.addToWarehouse(st3);
        if (wh.quantity(st3.getQuantity()) > 0) {
            wh.addToWarehouse(st3.getId(), st3.getName(), st3.getDescription(), st3.getPrice(), st3.getQuantity() - 1);
        } else {
            wh.addToWarehouse(st3);
            assertEquals(-1, wh.quantity(st3.getQuantity()));
        }
    }
}