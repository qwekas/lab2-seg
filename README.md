#SEG 

* Ove Tombak ovetombak@gmail.com
* Kalle J�geva kj1.maastik@gmail.com
* Karl-Johannes Kalma karljohannes.kalma@gmail.com

##Homework 1
* [Functional user stories](https://bitbucket.org/qwekas/lab2-seg/wiki/User%20stories)
* [Interview audio file](https://bitbucket.org/qwekas/lab2-seg/downloads/Interview.m4a)

##Homework 2
* [Use cases](https://bitbucket.org/qwekas/lab2-seg/wiki/Use%20cases)
* [Class model](https://bitbucket.org/qwekas/lab2-seg/wiki/Class%20model)

##Lab 4 - Development, Phase I
* [Task 3](https://bitbucket.org/qwekas/lab2-seg/wiki/Task%203)

##Lab 6 - Functional test plan
* [Task 3](https://docs.google.com/spreadsheets/d/1FROrzogKl1SEdR-1Ybvej6KcWXV4eBvDCts-Uut1Y7k/edit?ts=5a142351#gid=0)

##Lab 7 - Functional and Non-Functional Testing
* [Task 1-2.1](https://docs.google.com/document/d/1j1BDAbtptJ8rxfebYyNNrMs5fsDDyLMcmNbkNkEuFOg/edit#)
* [Report](https://docs.google.com/document/d/1WaFsiORZWiCfK55GsJ9-DO5kKhShB6auCBHcIEjpzpg/edit)
