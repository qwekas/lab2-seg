package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Sale;
import ee.ut.math.tvt.salessystem.logic.History;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryController implements Initializable {
    private static final Logger log = LogManager.getLogger(HistoryController.class);

    History history;
    private final HibernateSalesSystemDAO dao;
    @FXML
    private Button showBetweenDates;
    @FXML
    private Button showLast10;
    @FXML
    private Button showLast3Months;
    @FXML
    private DatePicker startDate;
    @FXML
    private DatePicker endDate;
    @FXML
    private TableView historyTableView;
    @FXML
    private TableView historyTableView2;

    public HistoryController(HibernateSalesSystemDAO dao) {
        this.dao = dao;
        this.history = new History(dao);
    }

    public void initialize(URL location, ResourceBundle resources) {
    }

    public static final LocalDate LOCAL_DATE (){
        LocalDate localDate = LocalDate.now();
        return localDate;
    }

    @FXML
    protected void showBetweenDatesButtonClicked() {
        log.info("Showing sales history between dates");
        try {
            display(history.getBetween(startDate.getValue(),endDate.getValue()));
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    @FXML
    protected void showLast10ButtonClicked() {
        log.info("Showing sales history of last 10 purchases");
        try {
            display(history.getLastTen());
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    @FXML
    protected void showLast3MonthsButtonClicked() {
        log.info("Showing sales history of last 3 months");
        try {
            startDate.setValue(LOCAL_DATE());
            endDate.setValue(LOCAL_DATE().minusMonths(3));
            display(history.getBetween(startDate.getValue(),endDate.getValue()));
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    public void display(List<Sale> list){
        historyTableView.setItems(new ObservableListWrapper<>(dao.getAllSales()));
        historyTableView.refresh();
    }
}
