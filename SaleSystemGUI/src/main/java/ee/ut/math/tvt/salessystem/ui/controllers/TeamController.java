package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class TeamController implements Initializable {
@FXML
private Label teamName;
@FXML
private Label teamLeader;
@FXML
private Label teamEmail;
@FXML
private Label teamMembers;

    private static final Logger log = LogManager.getLogger(TeamController.class);

    public void initialize(URL location, ResourceBundle resources) {
        teamData();
        log.info("Team data gathered!");
    }

    public void teamData(){
        final Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("../application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        teamName.setText(properties.getProperty("name"));
        teamLeader.setText(properties.getProperty("leader"));
        teamEmail.setText(properties.getProperty("email"));
        teamMembers.setText(properties.getProperty("members"));
    }
}


