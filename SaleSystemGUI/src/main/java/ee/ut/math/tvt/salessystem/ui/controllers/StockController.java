package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class StockController implements Initializable {
    private static final Logger log = LogManager.getLogger(StockController.class);

    private final SalesSystemDAO dao;

    @FXML
    private TableView<StockItem> warehouseTableView;
    @FXML
    private TextField barCode;
    @FXML
    private TextField amount;
    @FXML
    private TextField name;
    @FXML
    private TextField price;

    public StockController(SalesSystemDAO dao) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshStockItems();
    }

    @FXML
    private void addItemButtonClicked() {
        log.info("Item added to warehouse");
        addToWarehouse(new StockItem(Long.parseLong(barCode.getText()), name.getText(), "Empty atm",
                Double.parseDouble(price.getText()), Integer.parseInt(amount.getText())));
        //move to warehouse logic
        price.setText("");
        barCode.setText("");
        name.setText("");
        amount.setText("");
        refreshStockItems();
    }

    @FXML
    public void refreshButtonClicked() {
        refreshStockItems();
    }

    private void refreshStockItems() {
        warehouseTableView.setItems(new ObservableListWrapper<>(dao.findAllStockItems()));
        warehouseTableView.refresh();
        log.info("Warehouse table refreshed!");
    }


    public void addToWarehouse(StockItem item){
        dao.saveStockItem(item);
    }
}
